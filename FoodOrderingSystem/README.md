# Food Ordering System

### How to run this system?

Create **Waiter** instance with menu map. Menu map should contains available **CuisineType** as a keys, and list of **Items** as values.

### How to create a Dish for menu?

Use **DishesFactory** like this:

```java
DishesFactory factory = new DishesFactory.Builder()
    .ofCuisine(CuisineType.POLISH)
    .ofDish(DishType.DISH)
    .build();

polish.addAll(Arrays.asList(
    factory.createDish("Pierogi", Money.of("12.50")),
    factory.createDish("Gołąbki", Money.of("10.22")),
    factory.createDish("Bigos", Money.of("9.50")),
    factory.createDish("Kotlet schabowy", Money.of("15.50"))
));

```

### What waiter can do for you?

* Waiter can give you list of available cuisines and list of items for each of cuisine.
* Waiter allows you to order **Lunch** and **DrinkWithAdditions**
* Waiter returns you a list of ordered items.

### How to add new cuisine?

Add new value into **CuisineType** enum:
```java
public enum CuisineType {
    POLISH,
    MEXICAN,
    ITALIAN
}
```

### How to add new dish type?

Add new value into **DishType** enum:
```java
public enum DishType {
    DISH,
    DESSERT,
    DRINK,
    DRINK_ADDITION,
    DRINK_WITH_ADDITIONS,
    LUNCH
}
```

### How to add new dish type with some invariants?

Add new value into **DishType** enum and add new implementation of **Item** interface:
(look **Lunch** for example)
```java
public interface Item {

    Money getPrice();

    String getName();

    DishType getDishType();

    CuisineType getCuisineType();

}
```