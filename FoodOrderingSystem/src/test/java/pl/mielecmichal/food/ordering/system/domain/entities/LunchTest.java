package pl.mielecmichal.food.ordering.system.domain.entities;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class LunchTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void of_mainDishIsNotADish_shouldThrowException(){
        //given
        Dish main = new Dish(CuisineType.MEXICAN, DishType.DRINK_WITH_ADDITIONS, "Not a Main", Money.of("8.00"));
        Dish dessert = new Dish(CuisineType.MEXICAN, DishType.DESSERT, "Dessert", Money.of("8.00"));

        //then
        thrown.expect(IllegalArgumentException.class);

        //when
        Lunch.of(main, dessert);
    }

    @Test
    public void of_dessertIsNotADessert_shouldThrowException(){
        //given
        Dish main = new Dish(CuisineType.MEXICAN, DishType.DISH, "Dish", Money.of("8.00"));
        Dish dessert = new Dish(CuisineType.MEXICAN, DishType.DRINK, "Not a Dessert", Money.of("8.00"));

        //then
        thrown.expect(IllegalArgumentException.class);

        //when
        Lunch.of(main, dessert);
    }

    @Test
    public void getDishType_shouldAlwaysReturnsLunchType(){
        //given
        Dish correctMain = new Dish(CuisineType.MEXICAN, DishType.DISH, "Dish", Money.of("8.00"));
        Dish correctDessert = new Dish(CuisineType.MEXICAN, DishType.DESSERT, "Dessert", Money.of("8.00"));
        Lunch lunch = Lunch.of(correctMain, correctDessert);

        //when
        DishType type = lunch.getDishType();

        //then
        assertEquals(type, DishType.LUNCH);
    }

    @Test
    public void getName_correctItems_shouldReturnConcatenatedName() {
        //given
        String mainName = "Main";
        String dessertName = "Dessert";
        String expectedName = "Main and Dessert";

        Dish correctMain = new Dish(CuisineType.MEXICAN, DishType.DISH, mainName, Money.of("8.00"));
        Dish correctDessert = new Dish(CuisineType.MEXICAN, DishType.DESSERT, dessertName, Money.of("8.00"));
        Lunch lunch = Lunch.of(correctMain, correctDessert);

        //when
        String name = lunch.getName();

        //then
        Assert.assertEquals(name, expectedName);
    }

    @Test
    public void getPrice_correctItems_shouldReturnTotalItemsPrice() {
        //given
        Money mainPrice = Money.of("5.12");
        Money dessertPrice = Money.of("45.34");
        Money expectedPrice = Money.of("50.46");

        Dish correctMain = new Dish(CuisineType.MEXICAN, DishType.DISH, "Main", Money.of("5.12"));
        Dish correctDessert = new Dish(CuisineType.MEXICAN, DishType.DESSERT, "Dessert", Money.of("45.34"));
        Lunch lunch = Lunch.of(correctMain, correctDessert);

        //when
        Money price = lunch.getPrice();

        //then
        Assert.assertEquals(price, expectedPrice);
    }



}