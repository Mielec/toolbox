package pl.mielecmichal.food.ordering.system.domain;

import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.mielecmichal.food.ordering.system.domain.entities.Item;
import pl.mielecmichal.food.ordering.system.domain.factories.DishesFactory;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

public class WaiterTest {

    private Waiter waiter;

    private DishesFactory polishFactory;
    private DishesFactory italianFactory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        polishFactory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.POLISH)
                .ofDish(DishType.DISH)
                .build();

        italianFactory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.ITALIAN)
                .ofDish(DishType.DISH)
                .build();
    }

    @Test
    public void getCuisinesTypes_correctMenu_shouldReturnAllCuisines() {
        //given
        waiter = createCorrectWaiter();

        //when
        Set<CuisineType> result = waiter.getCuisinesTypes();

        //then
        assertThat(result).contains(CuisineType.POLISH, CuisineType.ITALIAN);
        assertThat(result).hasSize(2);
    }

    @Test
    public void getItemsForCuisine_correctMenu_shouldReturnAllItemsForCuisine() {
        //given
        waiter = createCorrectWaiter();

        //when
        List<Item> result = waiter.getItemsForCuisine(CuisineType.ITALIAN);

        //then
        assertThat(result).contains(italianFactory.createDish("Pizza Margherita", Money.of("12.50")));
        assertThat(result).hasSize(1);
    }

    @Test
    public void getItemsForCuisine_absentCuisine_shouldThrowException() {
        //given
        waiter = createCorrectWaiter();

        //then
        thrown.expect(IllegalArgumentException.class);

        //when
        waiter.getItemsForCuisine(CuisineType.MEXICAN);
    }

    @Test
    public void getItemsForCuisine_nullCuisine_shouldThrowException() {
        //given
        waiter = createCorrectWaiter();

        //then
        thrown.expect(NullPointerException.class);

        //when
        waiter.getItemsForCuisine(null);
    }

    @Test
    public void orderLunch_mainDishNameAbsentInMenu_shouldThrowException(){
        //given
        waiter = createCorrectWaiter();

        //then
        thrown.expect(IllegalArgumentException.class);

        //when
        waiter.orderLunch(CuisineType.POLISH, "BLA BLA BLA BLA", "Pączek");
    }

    @Test
    public void orderLunch_dessertNameAbsentInMenu_shouldThrowException(){
        //given
        waiter = createCorrectWaiter();

        //then
        thrown.expect(IllegalArgumentException.class);

        //when
        waiter.orderLunch(CuisineType.POLISH, "Pierogi", "BLA BLA BLA BLA");
    }


    @Test
    public void orderLunch_correctMainAndDessert_shouldAddToOrderList(){
        //given
        waiter = createCorrectWaiter();

        //when
        waiter.orderLunch(CuisineType.POLISH, "Pierogi", "Pączek");
        List<Item> result = waiter.getOrderedItems();

        //then
        assertThat(result).allMatch(item -> item.getName().equals("Pierogi and Pączek"));
        assertThat(result).hasSize(1);
    }


    private Waiter createCorrectWaiter() {
        List<Item> polish = new ArrayList<>();
        polish.addAll(Arrays.asList(
                polishFactory.createDish("Pierogi", Money.of("12.50")),
                polishFactory.createDish("Gołąbki", Money.of("10.22")),
                polishFactory.createDish("Bigos", Money.of("9.50")),
                polishFactory.createDish("Kotlet schabowy", Money.of("15.50"))
        ));

        polishFactory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.POLISH)
                .ofDish(DishType.DESSERT)
                .build();

        polish.addAll(Arrays.asList(
                polishFactory.createDish("Faworki", Money.of("4.50")),
                polishFactory.createDish("Pączek", Money.of("2.90")),
                polishFactory.createDish("Sernik", Money.of("1.50")),
                polishFactory.createDish("Krówki", Money.of("3.00"))
        ));

        List<Item> italian = new ArrayList<>();

        italian.addAll(Arrays.asList(
                italianFactory.createDish("Pizza Margherita", Money.of("12.50"))
        ));

        Map<CuisineType, List<Item>> menu = ImmutableMap.of(
                CuisineType.POLISH, polish,
                CuisineType.ITALIAN, italian
        );

        return new Waiter(menu);
    }

}