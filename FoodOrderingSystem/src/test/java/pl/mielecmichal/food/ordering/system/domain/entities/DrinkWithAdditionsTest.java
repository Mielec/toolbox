package pl.mielecmichal.food.ordering.system.domain.entities;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DrinkWithAdditionsTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void of_firstArgumentIsNotADrink_shouldThrowException() {
        //given
        Dish notDrink = new Dish(CuisineType.ITALIAN, DishType.DESSERT, "Name", Money.of("13.50"));
        List<Dish> correctAdditions = new ArrayList<>();

        //then
        thrown.expect(IllegalArgumentException.class);

        //when
        DrinkWithAdditions.of(notDrink, correctAdditions);
    }

    @Test
    public void of_wrongAddition_shouldThrowException() {
        //given
        Dish someDrink = new Dish(CuisineType.ITALIAN, DishType.DRINK, "Name", Money.of("13.50"));
        List<Dish> incorrectAdditions = Arrays.asList(
                new Dish(CuisineType.ITALIAN, DishType.DRINK, "Addition1", Money.of("13.50")),
                new Dish(CuisineType.ITALIAN, DishType.DRINK_ADDITION, "Addition2", Money.of("13.50"))
        );

        //then
        thrown.expect(IllegalArgumentException.class);

        //when
        DrinkWithAdditions.of(someDrink, incorrectAdditions);
    }

    @Test
    public void getPrice_correctItems_shouldReturnTotalPrice() {
        //given
        Money drinkPrice = Money.of("13.50");
        Money additionPrice = Money.of("5.45");
        Money totalExpected = Money.of("18.95");

        Dish someDrink = new Dish(CuisineType.ITALIAN, DishType.DRINK, "Name", drinkPrice);
        List<Dish> incorrectAdditions = Arrays.asList(
                new Dish(CuisineType.ITALIAN, DishType.DRINK_ADDITION, "Addition", additionPrice)
        );

        DrinkWithAdditions drink = DrinkWithAdditions.of(someDrink, incorrectAdditions);

        //when
        Money price = drink.getPrice();

        //then
        Assert.assertEquals(price, totalExpected);
    }

    @Test
    public void getName_correctItems_shouldReturnConcatenatedName() {
        //given
        String drinkName = "drinkName";
        String additionName = "additionName";
        String expectedName = "drinkName with additionName";

        Dish someDrink = new Dish(CuisineType.ITALIAN, DishType.DRINK, drinkName, Money.of("13.50"));
        List<Dish> incorrectAdditions = Arrays.asList(
                new Dish(CuisineType.ITALIAN, DishType.DRINK_ADDITION, additionName, Money.of("13.50"))
        );

        DrinkWithAdditions drink = DrinkWithAdditions.of(someDrink, incorrectAdditions);

        //when
        String name = drink.getName();

        //then
        Assert.assertEquals(name, expectedName);
    }

}