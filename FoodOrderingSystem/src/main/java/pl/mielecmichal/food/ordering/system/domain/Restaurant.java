package pl.mielecmichal.food.ordering.system.domain;

import com.google.common.collect.ImmutableMap;
import pl.mielecmichal.food.ordering.system.domain.entities.Item;
import pl.mielecmichal.food.ordering.system.domain.factories.DishesFactory;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Restaurant {
    private Map<CuisineType, List<Item>> menu;

    private Waiter waiter;

    public Restaurant() {
        menu = ImmutableMap.of(
                CuisineType.POLISH, configurePolishCuisine(),
                CuisineType.ITALIAN, configureItalianCuisine(),
                CuisineType.MEXICAN, configureMexicanCuisine());

        waiter = new Waiter(menu);
    }

    public Waiter getWaiter() {
        return waiter;
    }

    private List<Item> configurePolishCuisine() {
        List<Item> polish = new ArrayList<>();

        DishesFactory factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.POLISH)
                .ofDish(DishType.DISH)
                .build();

        polish.addAll(Arrays.asList(
                factory.createDish("Pierogi", Money.of("12.50")),
                factory.createDish("Gołąbki", Money.of("10.22")),
                factory.createDish("Bigos", Money.of("9.50")),
                factory.createDish("Kotlet schabowy", Money.of("15.50"))
        ));

        factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.POLISH)
                .ofDish(DishType.DESSERT)
                .build();

        polish.addAll(Arrays.asList(
                factory.createDish("Faworki", Money.of("4.50")),
                factory.createDish("Pączek", Money.of("2.90")),
                factory.createDish("Sernik", Money.of("1.50")),
                factory.createDish("Krówki", Money.of("3.00"))
        ));

        factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.POLISH)
                .ofDish(DishType.DRINK)
                .build();

        polish.addAll(Arrays.asList(
                factory.createDish("Kompot", Money.of("1.50")),
                factory.createDish("Oranżada", Money.of("1.90"))
        ));

        return polish;
    }

    private List<Item> configureItalianCuisine() {
        List<Item> italian = new ArrayList<>();

        DishesFactory factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.ITALIAN)
                .ofDish(DishType.DISH)
                .build();

        italian.addAll(Arrays.asList(
                factory.createDish("Pizza Margherita", Money.of("12.50")),
                factory.createDish("Gnocchi", Money.of("10.22")),
                factory.createDish("Spaghetti Carbonara", Money.of("9.50")),
                factory.createDish("Lasagna", Money.of("15.50"))
        ));


        factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.ITALIAN)
                .ofDish(DishType.DESSERT)
                .build();

        italian.addAll(Arrays.asList(
                factory.createDish("Tiramisù", Money.of("4.50")),
                factory.createDish("Panna cotta", Money.of("2.90")),
                factory.createDish("Crocetta of Caltanissetta", Money.of("1.50")),
                factory.createDish("Cannolo siciliano", Money.of("3.00"))
        ));

        factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.ITALIAN)
                .ofDish(DishType.DRINK)
                .build();

        italian.addAll(Arrays.asList(
                factory.createDish("Cappuccino", Money.of("8.50")),
                factory.createDish("Vino Piemonte", Money.of("7.90"))
        ));

        return italian;

    }

    private List<Item> configureMexicanCuisine() {
        List<Item> mexican = new ArrayList<>();

        DishesFactory factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.MEXICAN)
                .ofDish(DishType.DISH)
                .build();

        mexican.addAll(Arrays.asList(
                factory.createDish("Burrito", Money.of("12.50")),
                factory.createDish("Tacos", Money.of("10.22")),
                factory.createDish("Tortillas", Money.of("9.50")),
                factory.createDish("Caldo de queso", Money.of("15.50"))
        ));


        factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.ITALIAN)
                .ofDish(DishType.DESSERT)
                .build();

        mexican.addAll(Arrays.asList(
                factory.createDish("Tiramisù", Money.of("4.50")),
                factory.createDish("Tacos", Money.of("2.90")),
                factory.createDish("Tortillas", Money.of("1.50")),
                factory.createDish("Caldo de queso", Money.of("3.00"))
        ));

        factory = new DishesFactory.Builder()
                .ofCuisine(CuisineType.ITALIAN)
                .ofDish(DishType.DRINK)
                .build();

        mexican.addAll(Arrays.asList(
                factory.createDish("Cappuccino", Money.of("8.50")),
                factory.createDish("Vino Piemonte", Money.of("7.90"))
        ));

        return mexican;

    }

}
