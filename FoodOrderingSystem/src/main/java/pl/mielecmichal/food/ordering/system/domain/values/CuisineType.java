package pl.mielecmichal.food.ordering.system.domain.values;

public enum CuisineType {
    POLISH,
    MEXICAN,
    ITALIAN
}
