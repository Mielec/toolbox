package pl.mielecmichal.food.ordering.system.domain.factories;

import pl.mielecmichal.food.ordering.system.domain.entities.Dish;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

public class DishesFactory {

    private final CuisineType cuisineType;
    private final DishType dishType;

    DishesFactory(CuisineType cuisineType, DishType dishType) {
        this.cuisineType = cuisineType;
        this.dishType = dishType;
    }

    public Dish createDish(String name, Money price){
        return new Dish(cuisineType, dishType, name, price);
    }

    public static class Builder {

        private CuisineType cuisineType;
        private DishType dishType;

        public Builder ofCuisine(CuisineType cuisine) {
            this.cuisineType = cuisine;
            return this;
        }

        public Builder ofDish(DishType dish) {
            this.dishType = dish;
            return this;
        }

        public DishesFactory build() {
            return new DishesFactory(cuisineType, dishType);
        }
    }
}
