package pl.mielecmichal.food.ordering.system.domain.entities;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

import java.util.List;

public class DrinkWithAdditions implements Item {

    private final Dish drink;
    private final List<Dish> additions;

    private DrinkWithAdditions(Dish drink, List<Dish> additions) {
        this.drink = drink;
        this.additions = ImmutableList.copyOf(additions);
    }

    public static final DrinkWithAdditions of(Dish drink, List<Dish> additions) {
        Preconditions.checkArgument(Objects.equal(DishType.DRINK, drink.getDishType()));
        additions.forEach(d -> Preconditions.checkArgument(Objects.equal(DishType.DRINK_ADDITION, d.getDishType())));

        return new DrinkWithAdditions(drink, additions);
    }

    @Override
    public Money getPrice() {
        Money price = Money.of("0.00");
        price = price.add(drink.getPrice());
        for (Dish addition : additions) {
            price = price.add(addition.getPrice());
        }
        return price;
    }

    @Override
    public String getName() {
        String name = drink.getName() + " with ";
        for (Dish addition : additions) {
            name += addition.getName();
        }
        return name;
    }

    @Override
    public DishType getDishType() {
        return DishType.DRINK_WITH_ADDITIONS;
    }

    @Override
    public CuisineType getCuisineType() {
        return drink.getCuisineType();
    }
}
