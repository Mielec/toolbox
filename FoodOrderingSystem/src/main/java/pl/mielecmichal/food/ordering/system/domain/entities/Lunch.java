package pl.mielecmichal.food.ordering.system.domain.entities;

import com.google.common.base.Preconditions;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

public class Lunch implements Item {

    private final Dish mainDish;
    private final Dish dessert;

    private Lunch(Dish mainDish, Dish dessert) {
        this.mainDish = mainDish;
        this.dessert = dessert;
    }

    public static final Lunch of(Dish mainDish, Dish dessert) {
        Preconditions.checkArgument(mainDish.getDishType().equals(DishType.DISH));
        Preconditions.checkArgument(dessert.getDishType().equals(DishType.DESSERT));

        return new Lunch(mainDish, dessert);
    }

    public Dish getMainDish() {
        return mainDish;
    }

    public Dish getDessert() {
        return dessert;
    }

    public Money getPrice() {
        return mainDish.getPrice().add(dessert.getPrice());
    }

    public String getName() {
        return mainDish.getName() + " and " + dessert.getName();
    }

    public DishType getDishType() {
        return DishType.LUNCH;
    }

    public CuisineType getCuisineType() {
        return mainDish.getCuisineType();
    }

    @Override
    public String toString() {
        return getName();
    }
}
