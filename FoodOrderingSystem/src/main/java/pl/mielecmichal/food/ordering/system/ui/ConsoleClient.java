package pl.mielecmichal.food.ordering.system.ui;

import pl.mielecmichal.food.ordering.system.domain.Restaurant;
import pl.mielecmichal.food.ordering.system.domain.Waiter;
import pl.mielecmichal.food.ordering.system.domain.entities.Item;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class ConsoleClient {

    public static final void main(String[] args) {
        Restaurant restaurant = new Restaurant();
        Waiter waiter = restaurant.getWaiter();
        CuisineType cuisine = chooseCuisine(waiter);
        DishType dishType = chooseDishType();

        if (DishType.LUNCH.equals(dishType)) {
            chooseLunch(waiter, cuisine);
        }

        if (DishType.DRINK_WITH_ADDITIONS.equals(dishType)) {
            chooseDrink(waiter, cuisine);
        }

        List<Item> order = waiter.getOrderedItems();
        System.out.println("Your order: ");
        order.stream().peek(System.out::println).count();

        BigDecimal totalPrice = order.stream()
                .map(Item::getPrice)
                .map(Money::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Your order price is: " + totalPrice.toString());
    }

    private static void chooseLunch(Waiter waiter, CuisineType cuisine) {
        Item mainDish = chooseItem(waiter, cuisine, DishType.DISH, "main course");
        Item dessert = chooseItem(waiter, cuisine, DishType.DESSERT, "dessert");

        waiter.orderLunch(cuisine, mainDish.getName(), dessert.getName());
    }

    private static void chooseDrink(Waiter waiter, CuisineType cuisine) {
        Item drink = chooseItem(waiter, cuisine, DishType.DRINK, "drink");
        Item addition = chooseItem(waiter, cuisine, DishType.DRINK_ADDITION, "drink addition");

        waiter.orderDrink(cuisine, drink.getName(), Arrays.asList(addition.getName()));
    }

    private static CuisineType chooseCuisine(Waiter waiter) {
        List<CuisineType> cuisines = new ArrayList<>(waiter.getCuisinesTypes());
        ListChooser<CuisineType> chooser = new ListChooser("cuisine", cuisines);
        return chooser.ask();
    }

    private static DishType chooseDishType() {
        List<DishType> options = Arrays.asList(
                DishType.LUNCH,
                DishType.DRINK_WITH_ADDITIONS
        );

        ListChooser<DishType> listChooser = new ListChooser("item type", options);
        return listChooser.ask();
    }

    private static Item chooseItem(Waiter waiter, CuisineType cuisine, DishType dishType, String name) {
        List<Item> items = waiter.getItemsForCuisine(cuisine);

        List<Item> neededTypeItems = items.stream()
                .filter(i -> dishType.equals(i.getDishType()))
                .collect(toList());

        ListChooser<Item> chooser = new ListChooser(name, neededTypeItems);
        return (Item) chooser.ask();
    }

}
