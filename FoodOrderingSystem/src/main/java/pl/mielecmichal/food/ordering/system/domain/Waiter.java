package pl.mielecmichal.food.ordering.system.domain;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import pl.mielecmichal.food.ordering.system.domain.entities.Dish;
import pl.mielecmichal.food.ordering.system.domain.entities.Item;
import pl.mielecmichal.food.ordering.system.domain.entities.Lunch;
import pl.mielecmichal.food.ordering.system.domain.utilities.MyCollectors;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;

import java.util.*;

public class Waiter {

    private final Map<CuisineType, List<Item>> menu;

    private List<Item> orderedItems = new ArrayList<>();

    public Waiter(Map<CuisineType, List<Item>> menu) {
        this.menu = ImmutableMap.copyOf(menu);
    }

    public Set<CuisineType> getCuisinesTypes() {
        return menu.keySet();
    }

    public List<Item> getItemsForCuisine(CuisineType cuisineType) {
        Preconditions.checkNotNull(cuisineType);
        Preconditions.checkArgument(menu.containsKey(cuisineType));
        return ImmutableList.copyOf(menu.get(cuisineType));
    }

    public List<Item> getOrderedItems() {
        return ImmutableList.copyOf(orderedItems);
    }

    public void orderLunch(CuisineType cuisineType, String dishName, String dessertName) {
        Item dish = findItemInMenu(cuisineType, dishName, DishType.DISH);
        Item dessert = findItemInMenu(cuisineType, dessertName, DishType.DESSERT);
        orderedItems.add(Lunch.of((Dish) dish, (Dish) dessert));
    }

    public void orderDrink(CuisineType cuisineType, String drinkName, List<String> additionsNames) {
        Item drink = findItemInMenu(cuisineType, drinkName, DishType.DRINK);

        List<Item> drinkAdditions = new ArrayList<>();
        for (String additionName : additionsNames) {
            Item addition = findItemInMenu(cuisineType, additionName, DishType.DRINK_ADDITION);
            drinkAdditions.add(addition);
        }
    }

    private Item findItemInMenu(CuisineType cuisineType, String name, DishType dishType) {
        Preconditions.checkNotNull(cuisineType);
        Preconditions.checkNotNull(name);
        Preconditions.checkArgument(menu.containsKey(cuisineType));
        Preconditions.checkArgument(isItemPresentInMenu(cuisineType, name));

        Item item = menu.get(cuisineType).stream()
                .filter(i -> name.equals(i.getName()))
                .collect(MyCollectors.singleObject());

        Preconditions.checkArgument(Objects.equal(dishType, item.getDishType()));

        return item;
    }

    private boolean isItemPresentInMenu(CuisineType cuisineType, String name) {
        List<Item> items = getItemsForCuisine(cuisineType);
        return items.stream().anyMatch(i -> name.equals(i.getName()));
    }

}
