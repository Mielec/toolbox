package pl.mielecmichal.food.ordering.system.domain.entities;

import com.google.common.base.Preconditions;
import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

public final class Dish implements Item {

    private final CuisineType cuisineType;
    private final DishType dishType;
    private final String name;
    private final Money price;

    public Dish(CuisineType cuisineType, DishType dishType, String name, Money price) {
        this.cuisineType = cuisineType;
        this.name = name;
        this.price = price;
        this.dishType = dishType;
    }

    public static final Dish copyOf(Dish dish) {
        return new Dish(dish.getCuisineType(), dish.getDishType(), dish.getName(), dish.getPrice());
    }

    public CuisineType getCuisineType() {
        return cuisineType;
    }

    public DishType getDishType() {
        return dishType;
    }

    public String getName() {
        return name;
    }

    public Money getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return String.format("%s: %s(%s) %s", dishType, name, cuisineType, price);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dish dish = (Dish) o;

        if (cuisineType != dish.cuisineType) return false;
        if (dishType != dish.dishType) return false;
        if (name != null ? !name.equals(dish.name) : dish.name != null) return false;
        return price != null ? price.equals(dish.price) : dish.price == null;

    }

    @Override
    public int hashCode() {
        int result = cuisineType != null ? cuisineType.hashCode() : 0;
        result = 31 * result + (dishType != null ? dishType.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }
}
