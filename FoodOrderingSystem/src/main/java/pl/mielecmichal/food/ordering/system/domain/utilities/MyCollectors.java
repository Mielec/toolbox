package pl.mielecmichal.food.ordering.system.domain.utilities;

import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MyCollectors {

    public static <T> Collector<T, ?, T> singleObject() {
        return Collectors.collectingAndThen(
                Collectors.toList(),
                list -> {
                    if (list.size() != 1) {
                        throw new IllegalStateException();
                    }
                    return list.get(0);
                }
        );
    }

}
