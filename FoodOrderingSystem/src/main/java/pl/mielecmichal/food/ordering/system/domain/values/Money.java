package pl.mielecmichal.food.ordering.system.domain.values;

import java.math.BigDecimal;
import java.util.Currency;

public class Money {

    private final BigDecimal value;

    public static final Money of(String stringValue) {
        BigDecimal value = new BigDecimal(stringValue);
        return Money.of(value);
    }

    public static final Money of(BigDecimal value) {
        if (value.scale() != 2) {
            throw new IllegalArgumentException(value + "contains wronk number of decimal places.");
        }
        return new Money(value);
    }

    private Money(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Money add(Money augend) {
        BigDecimal value = this.getValue().add(augend.getValue());
        return Money.of(value);
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        return value != null ? value.equals(money.value) : money.value == null;

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}
