package pl.mielecmichal.food.ordering.system.domain.entities;

import pl.mielecmichal.food.ordering.system.domain.values.CuisineType;
import pl.mielecmichal.food.ordering.system.domain.values.DishType;
import pl.mielecmichal.food.ordering.system.domain.values.Money;

public interface Item {

    Money getPrice();

    String getName();

    DishType getDishType();

    CuisineType getCuisineType();

}
