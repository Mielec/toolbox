package pl.mielecmichal.food.ordering.system.domain.values;

public enum DishType {
    DISH,
    DESSERT,
    DRINK,
    DRINK_ADDITION,
    DRINK_WITH_ADDITIONS,
    LUNCH
}
