package pl.mielecmichal.food.ordering.system.ui;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Scanner;

public class ListChooser<E> {

    private final String name;
    private final List<E> options;

    public ListChooser(String name, List<? extends E> options) {
        this.options = ImmutableList.copyOf(options);
        this.name = name;
    }

    public E ask() {

        while (true) {
            System.out.println(String.format("Choose: %s", name));

            for (int i = 0; i < options.size(); i++) {
                System.out.println(String.format("%s. %s", i, options.get(i)));
            }

            Scanner in = new Scanner(System.in);
            int index = in.nextInt();

            if (index >= 0 && index < options.size()) {
                return options.get(index);
            }

            System.out.println(String.format("Incorrect number: ", index));
        }

    }

}
