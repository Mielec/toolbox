package pl.mielecmichal.minesweeper;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class LinearMineSweeperTest {

    private MineSweeper mineSweeper = new LinearMineSweeper();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    public static final String EMPTY_FIELD = "";
    public static final String FIELD_WITH_OPTIONAL_LINE_SEPARATOR_AT_THE_BEGINING = "\n*****\n.....\n.....\n.....\n";
    public static final String FIELD_WITHOUT_OPTIONAL_LINE_SEPARATOR_AT_THE_END = "*****\n.....\n.....\n.....";
    public static final String FIELD_WITH_DUPLICATED_LINE_SEPARATOR_AT_THE_BEGINING = "\n\n\n*****\n.....\n.....\n.....\n";
    public static final String FIELD_WITH_DUPLICATED_LINE_SEPARATOR_AT_THE_END = "*****\n.....\n.....\n.....\n\n";

    public static final String FIELD_WITH_INVALID_CHARACTERS = "*.???.*\n";
    public static final String FIELD_WITH_ROWS_OF_DIFFERENT_LENGTHS = ".....\n.....\n.....\n....\n";
    public static final String FIELD_WITH_EMPTY_ROW = ".....\n.....\n\n....\n";

    public static final String CORRECT_FIELD = "...*.\n" +
            "...*.\n" +
            "***..\n" +
            ".....\n";

    public static final String CORRECT_FIELD_ANSWER = "002*2\n" +
            "234*2\n" +
            "***21\n" +
            "23210\n";

    public static final String CORRECT_FIELD_2 = "*.....\n" +
            "*..*.*\n" +
            "......\n" +
            "......\n" +
            "***...\n" +
            "......";

    public static final String CORRECT_FIELD_2_ANSWER = "*21121\n" +
            "*21*2*\n" +
            "111121\n" +
            "232100\n" +
            "***100\n" +
            "232100";

    public static final String NO_MINE_FIELD = "..........\n" +
            "..........\n" +
            "..........\n" +
            "..........\n";

    public static final String NO_MINE_FIELD_ANSWER = "0000000000\n" +
            "0000000000\n" +
            "0000000000\n" +
            "0000000000\n";

    public static final String FULL_MINE_FIELD = "****\n" +
            "****\n" +
            "****\n" +
            "****\n";

    @Test
    @Parameters({
            EMPTY_FIELD,
            CORRECT_FIELD,
            FIELD_WITH_OPTIONAL_LINE_SEPARATOR_AT_THE_BEGINING,
            FIELD_WITHOUT_OPTIONAL_LINE_SEPARATOR_AT_THE_END,
            FIELD_WITH_DUPLICATED_LINE_SEPARATOR_AT_THE_BEGINING,
            FIELD_WITH_DUPLICATED_LINE_SEPARATOR_AT_THE_END

    })
    public void setMineField_correctFieldsGiven_shouldNotThrowException(String correctField) {
        //when
        mineSweeper.setMineField(correctField);
    }

    @Test
    @Parameters({
            FIELD_WITH_INVALID_CHARACTERS,
            FIELD_WITH_ROWS_OF_DIFFERENT_LENGTHS,
            FIELD_WITH_EMPTY_ROW
    })
    public void setMineField_inCorrectFieldGiven_shouldThrowException(String correctField) {
        expectedException.expect(IllegalArgumentException.class);

        //when
        mineSweeper.setMineField(correctField);
    }

    @Test
    public void setMineField_nullFieldGiven_shouldThrowException() {
        expectedException.expect(IllegalArgumentException.class);

        //when
        mineSweeper.setMineField(null);
    }

    @Test
    @Parameters({
            CORRECT_FIELD + "|" + CORRECT_FIELD_ANSWER,
            CORRECT_FIELD_2 + "|" + CORRECT_FIELD_2_ANSWER,
            NO_MINE_FIELD + "|" + NO_MINE_FIELD_ANSWER,
            FULL_MINE_FIELD + "|" + FULL_MINE_FIELD

    })
    public void getHintField_correctFieldGiven_shouldCountHints(String field, String answer) {
        //given
        mineSweeper.setMineField(field);

        //when
        String hints = mineSweeper.getHintField();

        //then
        assertEquals(answer, hints);
    }
}