package pl.mielecmichal.minesweeper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

/**
 * This class changes given mine field string into list of rows.
 */
public class LinearMineSweeper implements MineSweeper {

    private static final String MINE_FIELD = "\\*";
    private static final String EMPTY_FIELD = "\\.";
    private static final String LINE_SEPARATOR = "\\n";
    private static final Pattern ALLOWED_CHARACTERS_PATTERN = Pattern.compile("^[" + MINE_FIELD + EMPTY_FIELD + LINE_SEPARATOR + "]*$");


    private List<String> fieldRows = new ArrayList<>();
    private Integer rowLength;
    private Integer rowsCount;

    public void setMineField(String mineField) throws IllegalArgumentException {
        if (mineField == null) {
            throw new IllegalArgumentException("Mine Field is null");
        }

        if (!ALLOWED_CHARACTERS_PATTERN.matcher(mineField).matches()) {
            throw new IllegalArgumentException("Mine Field contains wrong characters");
        }

        List<String> rows = Arrays.asList(mineField.split(LINE_SEPARATOR));
        List<Integer> rowsLengths = rows.stream().map(String::length).collect(toList());

        Integer firstLineLength = rowsLengths.get(0);

        if (rowsLengths.stream().anyMatch(size -> !size.equals(firstLineLength))) {
            throw new IllegalArgumentException("Mine Field contains rows of different lengths");
        }

        fieldRows = rows;
        rowLength = firstLineLength;
        rowsCount = fieldRows.size();

    }

    public String getHintField() throws IllegalStateException {

        if (fieldRows.isEmpty()) {
            throw new IllegalStateException("Field was not initialized!");
        }

        Long[][] hints = new Long[rowsCount][rowLength];

        for (int coll = 0; coll < rowsCount; coll++) {
            for (int row = 0; row < rowLength; row++) {
                String allNeighbours = getAllNeighbours(coll, row);
                if (getCell(coll, row) != '*') {
                    hints[coll][row] = allNeighbours.chars().filter(c -> c == '*').count();
                } else {
                    hints[coll][row] = (long) -1;
                }

            }
        }

        StringBuilder result = new StringBuilder();
        for (int coll = 0; coll < rowsCount; coll++) {
            for (int row = 0; row < rowLength; row++) {
                result.append(hints[coll][row] != -1 ? hints[coll][row].toString() : '*');
            }
            result.append('\n');
        }

        result.setLength(result.length() - 1);

        return result.toString();
    }

    private String getAllNeighbours(int row, int coll) {

        StringBuffer allNeighbours = new StringBuffer();

        int leftBound = Math.max(coll - 1, 0);
        int rightBound = Math.min(coll + 1, rowLength - 1);
        int rightBoundExclusive = rightBound + 1;

        if (row != 0) {
            String previousRow = fieldRows.get(row - 1);
            allNeighbours.append(previousRow.substring(leftBound, rightBoundExclusive));
        }

        String currentRow = fieldRows.get(row);
        allNeighbours.append(currentRow.charAt(leftBound));
        allNeighbours.append(currentRow.charAt(rightBound));

        if (row != rowsCount - 1) {
            String nextRow = fieldRows.get(row + 1);
            allNeighbours.append(nextRow.substring(leftBound, rightBoundExclusive));
        }
        return allNeighbours.toString();
    }

    private char getCell(int row, int coll) {
        return fieldRows.get(row).charAt(coll);
    }
}
