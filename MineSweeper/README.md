# Mine Sweeper Solver

## Description

This small project contains utility for solving Mine Sweeper's field. It counts number of bombs at surrounding fields. Class accepts string describing mine field in following format:
```
*.....
*..*.*
......
......
***...
......
```

**Where**: Asterisk (*) is a bomb field and dot (.) is an empty field.

Returns number of surrounding bombs on each field as another string:

```
*21121
*21*2*
111121
232100
***100
232100
```

## Building

```
mvn clean install
```



